package com.example.dies;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MyActivity extends Activity {
    int no2;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Button sumar = (Button) findViewById(R.id.id_sumar);
        sumar.setOnClickListener(new View.OnClickListener() { public void onClick(View v) {
                suma( obtenervalor1(), obtenervalor2());
                }
        });
        Button restar = (Button) findViewById(R.id.restar);
        restar.setOnClickListener(new View.OnClickListener() { public void onClick(View v) {
                resta( obtenervalor1(), obtenervalor2());
            }
        });
        Button multiplicar = (Button) findViewById(R.id.multiplicar);
        multiplicar.setOnClickListener(new View.OnClickListener() { public void onClick(View v) {
                multiplicar(obtenervalor1(), obtenervalor2());
            }
        });
        Button dividir = (Button) findViewById(R.id.dividir);
        dividir.setOnClickListener(new View.OnClickListener() {public void onClick(View v) {
                dividir(obtenervalor1(),obtenervalor2());
            }
        });
    }

    public EditText obtenervalor1(){
        EditText valor1 = (EditText)findViewById(R.id.editText);
        return valor1;
    }

    public EditText obtenervalor2(){
        EditText valor2 = (EditText)findViewById(R.id.editText2);
        return valor2;
    }

    public void suma(EditText valor1,EditText valor2){
        try {
            toast(""+(EditText(valor1)+EditText(valor2)));
        }catch (Exception a){
            alertadedialogo("Error:\n\n"+a);
        } }

    public void resta(EditText valor1,EditText valor2){
        try {
            toast(""+(EditText(valor1)-EditText(valor2)));
        }catch (Exception a){
            alertadedialogo("Error:\n\n"+a);
        } }

    public void dividir(EditText valor1,EditText valor2){
        try {
            toast(""+EditText(valor1)/EditText(valor2));
        }catch (Exception a){
            alertadedialogo("Error:\n\n"+a);
        } }

    public void multiplicar(EditText valor1,EditText valor2){
        try {
            toast(""+EditText(valor1)*EditText(valor2));
        }catch (Exception a){
            alertadedialogo("Error:\n\n"+a);
        } }

    public int EditText(EditText valor1){
        String valor1string=valor1.getText().toString();
        return no2=Integer.parseInt(valor1string);
    }

    public void toast(String texo){
        Context contexto = getApplicationContext();
        int Duracion = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(contexto, texo, Duracion);
        toast.show();
    }

    private void alertadedialogo( String Mensaje ) {
        AlertDialog.Builder Dialogo = new AlertDialog.Builder(this);
        Dialogo.setTitle( "Error" )
                .setMessage(Mensaje)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }
}
